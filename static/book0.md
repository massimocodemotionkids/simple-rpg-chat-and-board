# La fucina dell'inferno

## Avventura Steampunk 


### Città con Porto e Tre Locande

Benvenuto nella città steampunk, con un vivace porto e tre locande uniche!

![Panorama della Città con Porto](./locations/city_with_ships_panorama.png)

#### Descrizione
La città è un crocevia di avventurieri, mercanti e ingegneri meccanici. Il porto è animato da navi volanti e vapore, mentre le tre locande offrono rifugio e intrattenimento.

## Locande
1. **Il Chiosco al porto di Geeno**
   
   Un'atmosfera da clima tropicale, un può fuoriluogo in questa città come il suo padrone Geeno. I cocktail sono però molto buoni 🍹

   ![La Catena d'Ingranaggi](./locations/tavern4.png)
   

2. **Il Calamaio Arrugginito**

   Un'atmosfera rustica con bevande speciali servite in tazze di metallo. Tavoli all'esterno e frutta fresca... se si ha certe tendenze naturalmente 😄

   ![Il Calamaio Arrugginito](./locations/tavern6.png)

3. **Il Giorno e la notte**

   Un'atmosfera che cambia completamente al tramontare del sole. Scegliete da che parte stare perché potreste diventare l'happy hour per alcune delle strane frequentazioni delle ore notturne. Serata a menù speciale con la luna piena... attenzione! 🌜

   |Di giorno... | Di notte... |
   |-------------|-------------|
   |![Il Giorno e la notte diurno](./locations/empty_reserved_tavern_daylight_variant.png) | ![Il Giorno e la notte notturno](./locations/empty_reserved_tavern_moonlight_variant.png) |

### Altre Location

#### La Casa del Botanico

Una dimora verde e rigogliosa, dove il botanico dedica il suo tempo a studiare le piante magiche.

![Casa del Botanico](./locations/house_of_botanist.png)

#### L'Edificio Ricco

Una sontuosa residenza con interni lussuosi.

![L'Edificio Ricco](./locations/rich_building_exterior.png)

##### Sotto Location

| Sala da Biliardo | Sala da Pranzo | Sala Lounge |
|-------|---------|---------------------|
| ![Sala da Biliardo](./locations/pool_table_room.png) | ![Sala da Pranzo](./locations/dinning_room.png) | ![Sala Lounge](./locations/lounge_rich_building.png)|
| Vista della città | Interno della locanda | Giardino del botanico |



### Elyos, la capitale

![Panorama di Elyos la capitale](./locations/city0.png)

Elyos, la capitale solar punk, è un luogo vibrante e pieno di energia. La città è caratterizzata da architetture innovative, alimentate da fonti di energia solare avanzate. Gli edifici risplendono sotto la luce del sole, creando un paesaggio urbano unico.

#### Caratteristiche Principali:

- **Architettura Solar Punk:** Gli edifici sono progettati con elementi sostenibili, pannelli solari e tecnologie avanzate, riflettendo la fusione tra estetica futuristica e sostenibilità ambientale.

- **Piazze e Spazi Verdi:** La città è arricchita da ampie piazze e spazi verdi, dove gli abitanti possono rilassarsi e godere della bellezza del paesaggio urbano.

- **Attività Culturali:** Elyos è un centro culturale, con teatri, musei e luoghi di intrattenimento che celebrano l'arte e l'innovazione.

- **Trasporti Avanzati:** Veicoli volanti e reti di trasporto avanzate rendono la mobilità fluida e efficiente, permettendo agli abitanti di spostarsi agilmente per la città.

Questa metropoli solar punk è il cuore pulsante di avventure e scoperte, offrendo ai giocatori un'ambientazione unica nel loro percorso attraverso il mondo steampunk.

## Personaggi

### Guerriero barbaro steampunk

![Guerriero barbaro steampunk](./avatars/warrior2.png)

#### Backstory

Nato nelle lande selvagge, questo guerriero steampunk ha abbracciato l'arte della battaglia, potenziando il proprio arsenale con ingegnose armi meccaniche.

#### Ruolo nel Gioco

Il guerriero è un abile combattente che può utilizzare armi avanzate e meccaniche per infliggere danni devastanti in battaglia.

### Fabbro

![Fabbro](./avatars/blacksmith.png)

#### Backstory
Apprendista di un antico maestro forgiaio, questo fabbro ha sviluppato una passione per la creazione di armi e armature uniche, abbellite con intricati dettagli steampunk.

#### Ruolo nel Gioco
Il fabbro è essenziale per l'equipaggiamento dei giocatori, potenziando e creando armi avanzate e gadget meccanici.

### Medico

![Medico donna](./avatars/medic.png)

#### Backstory
La giovane medico ha dedicato la sua vita allo studio della medicina steampunk, utilizzando cure avanzate e strumenti meccanici per guarire i feriti.

#### Ruolo nel Gioco
Il medico fornisce cure essenziali durante le avventure, curando ferite e fornendo potenti elisir di guarigione.

### Mercante goblin

![Mercante goblin](./avatars/goblin.png)

#### Backstory
Un astuto mercante goblin che ha viaggiato attraverso terre lontane, accumulando tesori e artefatti unici da vendere ai giocatori avventurieri.

#### Ruolo nel Gioco

Il mercante offre oggetti rari e utili, consentendo ai giocatori di acquistare equipaggiamenti avanzati e oggetti magici.

## Mappe disponibili

| Codici mappe |   |
|--------------------------|--------------------------|
| ``map0`` | ``map1``|
| ![map0](./maps/map0.png) | ![map1](./maps/map1.png) |
| ``map2`` | ``map3``|
| ![map2](./maps/map2.png) | ![map3](./maps/map3.png) |
| ``map5`` | ``map6``|
| ![map5](./maps/map5.png) | ![map6](./maps/map6.png) |