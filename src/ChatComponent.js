import { Peer } from "peerjs";

class ChatComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML = `
        <style>
            /* Stili per la chat-container e altri elementi */
        </style>
        <div id="chat-container">
            <ul id="chat-messages"></ul>
        </div>

        <div id="message-container">
            <input type="text" id="message-input" placeholder="Type your message...">
            <button id="send-button">Send</button>
        </div>`;

        // Inizializzazione del Peer
        this.peer = new Peer();
        this.peer.on('open', (id) => {
            //console.log('My peer ID is: ' + id);
            this.masterId = id; // Memorizziamo l'ID del Master quando è disponibile
            const masterIdSpan = document.querySelector('#master-id');

            if (masterIdSpan) {
                masterIdSpan.innerHTML = `<a href="http://localhost:1234?masterID=${id}" target="testing">Apri client</a>`;
            }

            this.connections = [];


            this.peer.on('connection', (conn) => {
                this.connections.push(conn);
                console.log("JUST CONNECTED: ", conn)
                conn.on('data', (data) => {
                    this.displayMessage(data);
                });
            });

        });


        // Gestione dell'invio dei messaggi
        this.shadowRoot.getElementById('send-button').addEventListener('click', () => this.sendMessage());



    }

    // Funzione per inviare messaggi
    sendMessage() {
        const messageInput = this.shadowRoot.getElementById('message-input');
        const message = messageInput.value;
        this.displayMessage(message);



        if (this.conn) {
            console.log("sending as player", this.conn);
            this.conn.send(message);
        } else {
            console.log("sending as master", this.connections)
            for (let connection of this.connections) {
                connection.send(message);
            }
        }
        messageInput.value = '';
    }

    // Funzione per visualizzare i messaggi nella chat
    displayMessage(message) {
        const chatMessages = this.shadowRoot.getElementById('chat-messages');
        const li = document.createElement('li');
        li.textContent = message;
        chatMessages.appendChild(li);
    }



    initializeWithMasterId(masterId) {
        // Connessione al Master
        this.conn = this.peer.connect(masterId);
    
        console.log("CONNECTING TO MASTER: ", this.conn);
    
        this.conn.on('open', () => {
            console.log("CONNECTED TO MASTER");
    
            this.conn.on('data', (data) => {
                console.log("DATA", data);
                this.displayMessage(data);
            });
    
            // Invia un messaggio al Master appena ci connettiamo
            this.conn.send('Connected as Player');
        });
    }
    
}

customElements.define('chat-component', ChatComponent);