// Importa la libreria PeerJS
import Peer from 'peerjs';
import { IndexedDBManager } from './IndexedDBManager';

//Markdown support
//import markdownit from 'markdown-it'
import { marked } from 'marked';

// Ottieni il parametro masterID dall'URL
const urlParams = new URLSearchParams(window.location.search);
const masterID = urlParams.get('masterID');

const createOrEnterRoom = (savedID, roomReadyCallback) => {
    // Crea un nuovo oggetto Peer
    const peer = new Peer(savedID);

    const MASTER = 0;
    const PLAYER = 1;

    const connections = [];
    const broadcast = (message) => {
        for (let connection of connections) {
            connection.send(message);
        }
        //store chat
        const chatContainer = document.querySelector(`#messages-container`);
        const chatLogData = {
            id: 'chat-log',
            campo: chatContainer.innerHTML
        }
        dbManager.updateData(chatLogData)
    }
    const userProfile = {
        type: MASTER,
        name: 'Master User',
        sendMessageFunction: broadcast,
        avatar: `avatar${Math.floor(Math.random() * 3)}.png`,
        peerID: null
    }

    const publishCard = (messageData) => {
        const chatContainer = document.querySelector(`#messages-container`);

        if (messageData.chatLog) {
            chatContainer.innerHTML = messageData.chatLog;
            return;
        }

        const newCard = document.createElement('div');
        newCard.classList.add('card');


        const messageObject = JSON.parse(messageData);

        if (messageObject.source == userProfile.peerID) {
            newCard.classList.add('myself');
        }

        let messageText = messageObject.text;

        // Utilizza una regex per trovare /d20 e sostituisci con un elemento span personalizzato
        messageText = messageText.replace(/\/d20/g, () => `<span class="d20" style="background-image: url(./icons/d20_outline.png)">${Math.floor(Math.random() * 20) + 1}</span>`);



        newCard.innerHTML = `
    <div class="avatar">
        <div class="picture"><img src="./${messageObject.avatar}"></div>
        <div class="user-name">${messageObject.alias}</div>
    </div>
    <div class="message">
        ${marked.parse(messageText)}
    </div>
    `;

        chatContainer.appendChild(newCard);

    }


    // Gestisci l'evento di apertura di una connessione
    peer.on('open', (peerID) => {
        // Controlla se è presente il parametro masterID nell'URL

        userProfile.peerID = peerID;

        if (roomReadyCallback && typeof roomReadyCallback == 'function') {
            roomReadyCallback(userProfile);
        }

        if (masterID) {
            userProfile.type = PLAYER;
            userProfile.name = `Player ${Math.floor(Math.random() * 10)}`;
            // Collegati al PeerJS ID specificato nel parametro masterID
            const connection = peer.connect(masterID);
            connection.on('open', () => {



                console.log(`Connesso a ${masterID}`);

                connection.on('data', (data) => {
                    console.log('message received from master', data)
                    publishCard(data);
                })

                // Puoi inviare dati attraverso questa connessione se necessario
                userProfile.sendMessageFunction = (message) => {
                    connection.send(message);
                }

            });

        } else {
            // Mostra l'ID ottenuto tramite peer.on('open')
            console.log(`Il tuo PeerJS ID è: ${peerID}`);
            document.querySelector('#room-link').setAttribute('href', `?masterID=${peerID}`);
            document.querySelector('#welcome').showModal();
            peer.on('connection', (conn) => {
                connections.push(conn);
                console.log(conn.on('open', () => {
                    getDataFromDB('chat-log', (data) => {
                        const messageData = {
                            source: userProfile.peerID,
                            chatLog: data.campo
                        }
                        conn.send(messageData)
                    })

                }))


                conn.on('data', (data) => {
                    console.log('message received from player', data);
                    publishCard(data);
                    userProfile.sendMessageFunction(data);
                })

            })

        }

        window.sendMessageToChat = (message) => {

            const messageData = {
                source: userProfile.peerID,
                alias: userProfile.name,
                text: message,
                avatar: userProfile.avatar
            }
            if (userProfile.type == MASTER) {
                publishCard(JSON.stringify(messageData));
            }


            userProfile.sendMessageFunction(JSON.stringify(messageData));
        }
    });

}

// Esempio di utilizzo:
const dbManager = new IndexedDBManager('nome_database', 'store_nome');


const dbCreationCallback = () => {
    //const newData = { id: 'chiave_unica', campo: 'valore ' + Math.floor(Math.random() * 99) };
    //return dbManager.updateData(newData);

    console.log("database ready");
    getDataFromDB('lastMasterSession', (data) => {

        if (!data) {
            console.log('no previous sessions');

            createOrEnterRoom(null, (userProfile) => {
                console.log(userProfile.peerID);
                if (!masterID) {
                    const newData = {
                        id: 'lastMasterSession',
                        campo: userProfile.peerID
                    }
                    dbManager.updateData(newData)
                }
            });
        } else {
            console.log('previous session', data.campo);
            createOrEnterRoom(data.campo, (userProfile) => {
                console.log(userProfile);
            });
        }

    });

    getDataFromDB('chat-log', (data) => {
        const chatContainer = document.querySelector(`#messages-container`);
        chatContainer.innerHTML = data.campo;
    })

}

const getDataFromDB = (key, callback) => {

    if (callback && typeof callback == 'function') {
        dbManager.getData(key).then(data => {
            callback(data);
        })
    } else {
        return dbManager.getData(key)
    }

}

if (!masterID) { //only masters have a session
    dbManager.openDatabase()
        .then(dbCreationCallback)
        .catch((error) => console.error(error));
} else {
    createOrEnterRoom();
}

const readManual = () => {
    fetch ('./book0.md').then((response) => {
        return response.text()
    }).then((data) => {
        
        document.querySelector('#open-manual').removeAttribute('disabled');
        const manualDialog = document.querySelector('#manual');
        const manualContent = document.querySelector('#manual-container');
        manualContent.innerHTML = marked.parse(data);        
        document.querySelector('#open-manual').addEventListener('click', ()=> {
            manualDialog.showModal();
        })
        document.querySelector('#close-manual').addEventListener('click', ()=> {
            manualDialog.close();
        })
    })
}

readManual();